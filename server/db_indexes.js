
Meteor.startup(function () {
    // Caspio Client records index
    ClientFlights._ensureIndex({PK_ID: 1}, {unique: true});
    Flights._ensureIndex({carrierCode: 1, flightNumber: 1, schdDepDate:1 }, {unique: true});
    Airlines._ensureIndex({IATA: 1}, {unique: true});
    Airports._ensureIndex({IATA_FAA: 1}, {unique: true});
});