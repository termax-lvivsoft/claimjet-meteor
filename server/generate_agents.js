Meteor.methods({
    'generateAgents': function (amount) {
        if (!amount) return;

        var existAgentsCount = Credentials.find().count();

        for (var i = 0; i < amount; i++) {
            // increment Agent number
            existAgentsCount++;
            var username = "agent" + existAgentsCount;
            var password = Meteor.hashid();

            id = Accounts.createUser({
                //email: user.email,
                username: username,
                password: password,
                roles: ['operator'],
                profile: {name: username}
            });

            Roles.addUsersToRoles(id, ['operator']);

            Credentials.insert({username: username, password: password, userId: id});
        }

    },
    'generateAgent': function (username) {
        if (!username) return;

        var password = Meteor.hashid();
        id = Accounts.createUser({
            //email: user.email,
            username: username,
            password: password,
            roles: ['operator'],
            profile: {name: username}
        });

        Roles.addUsersToRoles(id, ['operator']);

        Credentials.insert({username: username, password: password, userId: id});

    }
});