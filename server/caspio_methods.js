var caspio = require('./connectors/caspio-connector');

Meteor.methods({
    'caspioGetRecords': function () {
        console.log("Start Client Flight Records Extraction");

        var importedCount = 0;

        var caspio_query = {
            select: '*'
        };

        //  Unblock Async
        this.unblock();

        var caspioWrapped = Meteor.wrapAsync(caspio.getRecords);
        var caspioRecords = caspioWrapped('contacts', caspio_query);

        if (caspioRecords) {
            for (var i = 0; i < caspioRecords.length; i++) {
                var skipped = false;

                try {
                    ClientFlights.insert(caspioRecords[i]);
                } catch (e) {
                    console.log(e);
                    skipped = true;
                }
                if (!skipped) {
                    importedCount++;
                    console.log("Imported: ", importedCount)
                }

            }
        }

    }
});