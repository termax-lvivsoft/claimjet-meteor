var async = require("async");
var util = require("util");

//var connection = mysql.createConnection();

var db = {
    host: 'flightsv2.c8ilyysr4mde.us-east-1.rds.amazonaws.com',
    user: 'termax',
    port: 3306,
    password: 'fuckdat123',
    database: 'flights'
};

//connection.connect(function (err) {
//      if (err) {
//          console.error('error connecting to db: ' + err.stack);
//          return;
//      }
//
//      console.log('connected to db as id ' + connection.threadId);
//  }
//);

//connection.query('SELECT * FROM all_flights LIMIT 1', function (err, rows, fields) {
//    if (err) console.log(err);
//    console.log(rows);
//});


Meteor.methods({
    //'getTourRecords': function () {
    //    console.log("Start Tour Records Extraction");
    //
    //    //  Unblock Async
    //    this.unblock();
    //
    //    // init imported counter
    //    var importedCount = 0;
    //
    //    var step = 100;
    //
    //    // Wrap Needed async functions
    //    var connectDB = Meteor.wrapAsync(connection.connect, connection);
    //    var endDB = Meteor.wrapAsync(connection.end, connection);
    //    var getQuery = Meteor.wrapAsync(connection.query, connection);
    //
    //    // Connect to DB
    //    connectDB();
    //
    //    // Run Query to get amount of records
    //    var countQueryResults = getQuery('SELECT COUNT(*) FROM tour_records');
    //    var resultsCount = countQueryResults && countQueryResults[0]['COUNT(*)'] || 0;
    //
    //    // put all results in MONGO
    //    for (var c = 0; c < resultsCount; c = c + step) {
    //        var queryResults = getQuery('SELECT * FROM tour_records LIMIT ' + c + ', ' + step);
    //        //console.log(queryResults);
    //        for (var i = 0; i < queryResults.length; i++) {
    //            TourRecords.insert({
    //                firstName: queryResults[i].FName,
    //                lastName: queryResults[i].LName,
    //                primaryNumber: queryResults[i].PrimaryNo,
    //                destTo: queryResults[i].DestTo,
    //                destFrom: queryResults[i].DestFrom,
    //                bookingDate: queryResults[i].BookingDate,
    //                carrier: queryResults[i].Airline,
    //                flightNo: queryResults[i].sge_Flightno,
    //                travelDate: queryResults[i].TravelDate
    //            });
    //            importedCount++;
    //            console.log(importedCount)
    //        }
    //    }
    //    connection.end();
    //    console.log(resultsCount);
    //
    //    return resultsCount;
    //
    //
    //},
    //
    //'oag': function () {
    //    this.unblock();
    //    var connectDB = Meteor.wrapAsync(connection.connect, connection);
    //    connectDB();
    //    var getQuery = Meteor.wrapAsync(connection.query, connection);
    //    queryResults = getQuery('SELECT * FROM all_flights LIMIT 1');
    //    connection.end();
    //    console.log(queryResults);
    //    return queryResults;
    //    //connection.query('SELECT 1 FROM all_flights', function (err, rows, fields) {
    //    //    if (err) throw err;
    //    //    console.log(rows);
    //    //    return rows;
    //    //});
    //
    //},
    //'oag2': function () {
    //    connection.query('SELECT * FROM all_flights LIMIT 1', function (err, rows, fields) {
    //        if (err) throw err;
    //        console.log(rows);
    //        return rows;
    //    });
    //},
    'getFlights': function () {
        console.log("Start Flights Extraction");
        var uniqueClientFlights = ClientFlights.aggregate(
          [
              {
                  "$group": {
                      "_id": {
                          airline: "$airline",
                          flight_number: "$flight_number",
                          scheduled_departure_date: "$scheduled_departure_date"
                      }
                  }
              }
          ]
        );

        //  Unblock Async
        this.unblock();

        var connection = mysql.createConnection(db);

        // Wrap Needed async functions

        var connectDB = Meteor.wrapAsync(connection.connect, connection);
        var endDB = Meteor.wrapAsync(connection.end, connection);
        var getQuery = Meteor.wrapAsync(connection.query, connection);

        // wrap each
        var syncEach = Meteor.wrapAsync(async.eachSeries);

        // Iterator
        function flightIterator(record, cb) {
            if (!record) return cb();
            if (!record._id.airline || !record._id.flight_number || !record._id.scheduled_departure_date) {
                console.log("Corrupt client flight record:\n", record);
                return cb();
            }
            // cut the 00 time from date
            var schdDepDate = record._id.scheduled_departure_date.slice(0, 10);

            var db1 = "all_flights";
            var db2 = "client_flights_oag";

            var query = 'SELECT * FROM %s WHERE carrierCode = "' + record._id.airline +
              '" AND flightNumber = "' + record._id.flight_number +
              '" AND schdDepDate LIKE "' + schdDepDate +
              '%" LIMIT 1';

            //console.log(query);

            //console.log(record._id.airline);
            var QueryResults = getQuery(util.format(query, db1));

            if (!QueryResults.length) {
                QueryResults = getQuery(util.format(query, db2));
            }

            if (!QueryResults.length) {
                try {
                    Flights.insert({
                        carrierCode: record._id.airline,
                        flightNumber: record._id.flight_number,
                        schdDepDate: schdDepDate,
                        missing: true
                    });
                } catch (e) {
                    console.log(e);
                }
            } else {
                try {
                    Flights.insert(QueryResults[0]);
                } catch (e) {
                    console.log(e);
                }
            }

            cb();
        }

        // error handler
        function flightError(err) {
            if (err) {
                console.log(err)
            } else {
                console.log("Finished processing all flights")
            }
        }

        // Connect to DB
        connectDB();

        // Run queries for each unique client flight
        syncEach(uniqueClientFlights, flightIterator, flightError);

        endDB(dbEndCb);
        //connection.end(dbEndCb)

    },
    'getAirlines': function () {
        console.log("Start Airlines Extraction");
        var uniqueAirlines = ClientFlights.aggregate(
          [
              {
                  "$group": {
                      "_id": {
                          airline: "$airline"
                      }
                  }
              }
          ]
        );

        //  Unblock Async
        this.unblock();

        var connection = mysql.createConnection(db);

        // Wrap Needed async functions

        var connectDB = Meteor.wrapAsync(connection.connect, connection);
        var endDB = Meteor.wrapAsync(connection.end, connection);
        var getQuery = Meteor.wrapAsync(connection.query, connection);

        // wrap each
        var syncEach = Meteor.wrapAsync(async.eachSeries);

        // Iterator
        function airlineIterator(record, cb) {
            if (!record) return cb();
            if (!record._id.airline) {
                console.log("Corrupt client flight record:\n", record);
                return cb();
            }

            var db1 = "AIRLINES";

            var query = 'SELECT * FROM %s WHERE IATA = "' + record._id.airline +
              '" LIMIT 1';

            //console.log(query);

            //console.log(record._id.airline);
            var QueryResults = getQuery(util.format(query, db1));

            if (!QueryResults.length) {
                try {
                    Airlines.insert({IATA: record._id.airline, missing: true});
                } catch (e) {
                    console.log(e)
                }
            } else {
                try {
                    Airlines.insert(QueryResults[0]);
                } catch (e) {
                    console.log(e)
                }
            }

            cb();
        }

        // error handler
        function airlineError(err) {
            if (err) {
                console.log(err)
            } else {
                console.log("Finished processing all airlines")
            }
        }

        // Connect to DB
        connectDB();

        // Run queries for each unique client flight
        syncEach(uniqueAirlines, airlineIterator, airlineError);

        endDB(dbEndCb);

    },
    'getAirports': function () {
        console.log("Start Airports Extraction");
        var uniqueDepAirports = Flights.aggregate(
          [
              {
                  "$group": {
                      "_id": {
                          aptCode: "$depAptCode"
                      }
                  }
              }
          ]
        );
        var uniqueArrAirports = Flights.aggregate(
          [
              {
                  "$group": {
                      "_id": {
                          aptCode: "$arrAptCode"
                      }
                  }
              }
          ]
        );

        var flatDepAirports = [];
        var flatArrAirports = [];

        _.each(uniqueDepAirports, function (value) {
            flatDepAirports.push(value._id.aptCode)
        });

        _.each(uniqueArrAirports, function (value) {
            flatArrAirports.push(value._id.aptCode)
        });

        // merge and dedupe depApt and arrApt
        var uniqueAirports = _.union(flatDepAirports, flatArrAirports);
        //  Unblock Async
        this.unblock();

        var connection = mysql.createConnection(db);

        // Wrap Needed async functions

        var connectDB = Meteor.wrapAsync(connection.connect, connection);
        var endDB = Meteor.wrapAsync(connection.end, connection);
        var getQuery = Meteor.wrapAsync(connection.query, connection);

        // wrap each
        var syncEach = Meteor.wrapAsync(async.eachSeries);

        // Iterator
        function airportIterator(record, cb) {
            if (!record) return cb();
            if (!record) {
                console.log("Corrupt flight record:\n", record);
                return cb();
            }

            var db1 = "AIRPORTS";

            var query = 'SELECT * FROM %s WHERE IATA_FAA = "' + record +
              '" LIMIT 1';

            //console.log(query);

            //console.log(record._id.airline);
            var QueryResults = getQuery(util.format(query, db1));

            if (!QueryResults.length) {
                try {
                    Airports.insert({IATA_FAA: record, missing: true});
                } catch (e) {
                    console.log(e)
                }
            } else {
                try {
                    Airports.insert(QueryResults[0]);
                } catch (e) {
                    console.log(e)
                }
            }
            cb();
        }

        // error handler
        function airportError(err) {
            if (err) {
                console.log(err)
            } else {
                console.log("Finished processing all airports")
            }
        }

        // Connect to DB
        connectDB();

        // Run queries for each unique client flight
        syncEach(uniqueAirports, airportIterator, airportError);

        endDB(dbEndCb);

    }
});

function dbEndCb(err) {
    // The connection, table events and queries are terminated now
    if (err) {
        console.log(err)
    } else {
        console.log("mySQL connection closed")
    }
}