Slingshot.createDirective("images", Slingshot.S3Storage, {
    acl: "public-read",
    //acl: "private",
    region: "us-west-1",

    authorize: function () {
        if (!this.userId) {
            var message = "Please login before posting images";
            throw new Meteor.Error("Login Required", message);
        }

        return true;
    },

    key: function (file) {
        //Store file into a directory by the user's username.
        var user = Meteor.users.findOne(this.userId);
        //console.log("user", user);
        return "uploads/" + Meteor.uuid();
    }

});