var request = require('request');
var async = require('async');
var logger = require('winston').loggers.get('default_logger');


// Base64 encoded Client ID + ':' + Client secret
var CASPIO_ACCESS_TOKEN = "NDk2NTQ3MjM2NGNmNGRjNWY4ODc0YjUyMzA0ZTVkZjQ1ZDcxMzQwNDU4YjM1NzFjNGU6MmE1YTM2MWE1YWIyNGJmM2I2ZmIxOGYxYjQzM2RmM2M0MDY3MjQ2MjdiYjY5MGI4NDk=";

var CASPIO_TOKENS = null;


caspio_init(function (err) {
    if (err) {
        logger.error(err.toString());
        throw err;
    } else logger.info('Fetched Caspio bearer token');
});

function caspio_init(callback) {

    logger.info('Trying to fetch Caspio bearer token...');

    request.post("https://c2bkr113.caspio.com/oauth/token", {
        body: "grant_type=client_credentials",
        headers: {
            Authorization: 'Basic ' + CASPIO_ACCESS_TOKEN
        }
    }, function (err, response, body) {

        if (response && response.statusCode != '200')
            return callback(new Error("Failed to authenticate Caspio client. HTTP " + response.statusCode));

        var parsedBody = null;

        try {
            parsedBody = JSON.parse(body);
        } catch (e) {
            return callback(new Error("Failed to authenticate Caspio client. Can not parse response: " + body));
        }

        if (!parsedBody || !parsedBody.access_token || !parsedBody.access_token) {
            callback(new Error("Failed to authenticate Caspio client. Unknown response format: " + body));
        } else {

            CASPIO_TOKENS = {
                access_token: parsedBody.access_token,
                refresh_token: parsedBody.refresh_token
            };

            callback(null);
        }
    });
}


function caspio_refresh_token(callback) {

    logger.info('Trying to refresh Caspio bearer token');

    request.post("https://c2bkr113.caspio.com/oauth/token", {
        body: "grant_type=client_credentials&refresh_token=" + CASPIO_TOKENS.refresh_token,
        headers: {
            Authorization: 'Basic ' + CASPIO_ACCESS_TOKEN
        }
    }, function (err, response, body) {

        if (err) logger.error("https://c2bkr113.caspio.com/oauth/token " + err.toString());

        // refresh token expired. need to reauth from scratch
        if (response && response.statusCode == '401')
            return caspio_init(callback);

        if (response && response.statusCode != '200')
            return callback(new Error("Failed to refresh Caspio OAuth token. HTTP " + response.statusCode));

        var parsedBody = null;
        try {
            parsedBody = JSON.parse(body);
        } catch (e) {
            return callback(new Error("Failed to refresh Caspio OAuth token. Can not parse response: " + body));
        }

        if (!parsedBody || !parsedBody.access_token || !parsedBody.access_token)
            callback(new Error("Failed to refresh Caspio OAuth token. Unknown response format: " + body));

        CASPIO_TOKENS = {
            access_token: parsedBody.access_token,
            refresh_token: parsedBody.refresh_token
        }
    });
}

module.exports.getRecords = function getRecord(tableName, sqlQueryObject, callback) {

    if (typeof callback != 'function') return;
    var error;

    if (!CASPIO_TOKENS) {
        error = new Error("Caspio client is not ready");
        error.statusCode = 500;
        return callback(error);
    }

    var encodedQueryString = null;

    try {
        encodedQueryString = sqlQueryObject && encodeURIComponent(JSON.stringify(sqlQueryObject));
    } catch (e) {
        error = new Error("Failed to serialize SQL query");
        error.statusCode = 500;
        return callback(error);
    }

    var url = 'https://c2bkr113.caspio.com/rest/v1/tables/' + tableName + '/rows';
    if (encodedQueryString) url += '?q=' + encodedQueryString;

    request.get(url, {
        headers: {
            Authorization: 'Bearer ' + CASPIO_TOKENS.access_token
        }
    }, function (err, response, body) {


        if (err) logger.error(url + " " + err.toString());

        if (response && response.statusCode == 401) {
            return caspio_refresh_token(function (err) {
                if (err) logger.error(err.toString());
                else getRecords(tableName, sqlQueryObject, callback);
            })
        }
        try {
            body = JSON.parse(body);
        } catch (e) {

        }

        var result = body && body.Result;

        if (!result) return callback('Failed to get record');
        else callback(null, result);
    });
};

module.exports.getRecord = function getRecord(tableName, sqlQueryObject, callback) {

    if (typeof callback != 'function') return;
    var error;

    if (!CASPIO_TOKENS) {
        error = new Error("Caspio client is not ready");
        error.statusCode = 500;
        return callback(error);
    }

    var encodedQueryString = null;

    try {
        encodedQueryString = sqlQueryObject && encodeURIComponent(JSON.stringify(sqlQueryObject));
    } catch (e) {
        error = new Error("Failed to serialize SQL query");
        error.statusCode = 500;
        return callback(error);
    }

    var url = 'https://c2bkr113.caspio.com/rest/v1/tables/' + tableName + '/rows';
    if (encodedQueryString) url += '?q=' + encodedQueryString;

    request.get(url, {
        headers: {
            Authorization: 'Bearer ' + CASPIO_TOKENS.access_token
        }
    }, function (err, response, body) {


        if (err) logger.error(url + " " + err.toString());

        if (response && response.statusCode == 401) {
            return caspio_refresh_token(function (err) {
                if (err) logger.error(err.toString());
                else getRecord(tableName, sqlQueryObject, callback);
            })
        }
        try {
            body = JSON.parse(body);
        } catch (e) {

        }

        var record = body && body.Result && body.Result[0];

        if (!record) return callback('Failed to get record');
        else callback(null, record);
    });
};


module.exports.getViewRecord = function getViewRecord(viewName, sqlQueryObject, callback) {

    if (typeof callback != 'function') return;
    var error;

    if (!CASPIO_TOKENS) {
        error = new Error("Caspio client is not ready");
        error.statusCode = 500;
        return callback(error);
    }

    var encodedQueryString = null;

    try {
        encodedQueryString = sqlQueryObject && encodeURIComponent(JSON.stringify(sqlQueryObject));
    } catch (e) {
        error = new Error("Failed to serialize SQL query");
        error.statusCode = 500;
        return callback(error);
    }

    var url = 'https://c2bkr113.caspio.com/rest/v1/views/' + viewName + '/rows';
    if (encodedQueryString) url += '?q=' + encodedQueryString;

    request.get(url, {
        headers: {
            Authorization: 'Bearer ' + CASPIO_TOKENS.access_token
        }
    }, function (err, response, body) {

        if (err) logger.error(url + " " + err.toString());

        if (response && response.statusCode == 401) {
            return caspio_refresh_token(function (err) {
                if (err) logger.error(err.toString());
                else getViewRecord(viewName, sqlQueryObject, callback);
            })
        }

        try {
            body = JSON.parse(body);
        } catch (e) {

        }
        var record = body && body.Result && body.Result[0];

        if (!record) return callback('Failed to get record');
        else callback(null, record);
    });
};


module.exports.insertRecord = function insertRecord(tableName, jsonRowData, callback) {

    if (typeof callback != 'function') return;
    var error;

    if (!CASPIO_TOKENS) {
        error = new Error("Caspio client is not ready");
        error.statusCode = 500;
        return callback(error);
    }

    if (!tableName || !jsonRowData) {
        error = new Error("Bad insert parameters");
        error.statusCode = 400;
        return callback(error);
    }

    var url = 'https://c2bkr113.caspio.com/rest/v1/tables/' + tableName + '/rows';

    request.post(url, {
        json: jsonRowData,
        headers: {
            "Authorization": 'Bearer ' + CASPIO_TOKENS.access_token,
            "Content-Type": "application/json"
        }
    }, function (err, response, body) {

        if (response && response.statusCode == 401) {
            return caspio_refresh_token(function (err) {
                if (err) logger.error(err.toString());
                else insertRecord(tableName, jsonRowData, callback);
            })
        }

        if (err || !response) {
            logger.error(url + " " + err.toString());
            return callback(err);
        }

        if (response.statusCode != 201) return callback(new Error("HTTP " + response.statusCode + " " + body));

        callback(null, body);
    });
};


module.exports.insertViewRecord = function insertViewRecord(viewName, jsonRowData, callback) {


    if (typeof callback != 'function') return;
    var error;

    if (!CASPIO_TOKENS) {
        error = new Error("Caspio client is not ready");
        error.statusCode = 500;
        return callback(error);
    }

    if (!viewName || !jsonRowData) {
        error = new Error("Bad insert parameters");
        error.statusCode = 400;
        return callback(error);
    }

    var url = 'https://c2bkr113.caspio.com/rest/v1/views/' + viewName + '/rows';

    request.post(url, {
        json: jsonRowData,
        headers: {
            "Authorization": 'Bearer ' + CASPIO_TOKENS.access_token,
            "Content-Type": "application/json"
        }
    }, function (err, response, body) {

        if (response && response.statusCode == 401) {
            return caspio_refresh_token(function (err) {
                if (err) logger.error(err.toString());
                else insertViewRecord(viewName, jsonRowData, callback);
            })
        }

        if (err || !response) {
            logger.error(url + " " + err.toString());
            return callback(err);
        }

        if (response.statusCode != 201) return callback(new Error("HTTP " + response.statusCode + " " + body));

        callback(null, body);
    });
};