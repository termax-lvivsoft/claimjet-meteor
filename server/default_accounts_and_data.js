if (Meteor.users.find().fetch().length === 0) {

    console.log('Creating users: ');

    var users = [
        {name: "Maxim Tereshko", username: "termax", roles: ['admin', 'operator']},
        {name: "ClaimJet Administrator", username: "admin", roles: ['admin']},
        {name: "ClaimJet Manager", username: "manager", roles: ['manager']},
        {name: "ClaimJet Restricted", username: "test"}
        //{name: "ClaimJet Agent One", username: "agent1", roles: ['operator']}
        //{name: "Maxim Tereshko", email: "termax@nton.info", roles: ['admin']},
        //{name: "ClaimJet Developer", email: "dev@claimjet.com", roles: ['admin']},
        //{name: "ClaimJet Operator", email: "op@claimjet.com", roles: ['operator']}
    ];

    var env = Meteor.settings.ENV;
    var message = "*Fresh agent server started on " + env +" with the following users:* \n";


    var usersCreated = "";

    _.each(users, function (user) {
        var id;

        var userToCreate = {
            //email: user.email,
            username: user.username,
            password: Meteor.hashid(),
            profile: {name: user.name}
        };

        id = Accounts.createUser(userToCreate);

        userToCreate.roles = user.roles;
        usersCreated += JSON.stringify(userToCreate) + "\n";


        console.log('added user: ' + user.name + ' id: ' + id);
        if (user.roles && user.roles.length > 0) {
            // Need _id of existing user record so this call must come
            // after `Accounts.createUser` or `Accounts.onCreate`
            Roles.addUsersToRoles(id, user.roles);
        }
    });

    Meteor.call("sendSlackMessage", message + usersCreated);

} else {
    // Prevent new user creation via interface. Has to be restarted after initial launch to take effect.
    //console.log(" \n  === Blocking new user creation === ");
    //Accounts.validateNewUser(function() {
    //    return false;
    //});

}

Accounts.validateNewUser(function (user) {
    var loggedInUser = Meteor.user();

    if (Roles.userIsInRole(loggedInUser, ['admin'])) {
        // NOTE: This example assumes the user is not using groups.
        return true;
    }

    throw new Meteor.Error(403, "Not authorized to create new users");
});

