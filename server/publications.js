Meteor.publish("clients",       function(){ return Clients.find(); });
Meteor.publish("flights",       function(){ return Flights.find(); });
Meteor.publish("clientForms",   function(){ return ClientForms.find(); });
Meteor.publish("tourRecords",   function(){ return TourRecords.find(); });
Meteor.publish("clientFlights",   function(){ return ClientFlights.find(); });
Meteor.publish("images",   function(){ return Images.find(); });
Meteor.publish("airlines",   function(){ return Airlines.find(); });
Meteor.publish("airports",   function(){ return Airports.find(); });


Meteor.publish('credentials', function () {
    if (Roles.userIsInRole(this.userId, ['admin'])) {

        return Credentials.find({});

    } else {

        // user not authorized. do not publish secrets
        this.stop();

    }
});

