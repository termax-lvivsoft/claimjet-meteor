//AutoForm.debug();

// session variables:
//
// newMode
// editMode
// clientPhoneNumber
// selectedFlightId
// selectedClaimId
// preFillObj
// insertData


Template.agentFlow.helpers({
    newMode: function () {
        return Session.get('newMode');
    },
    editMode: function () {
        return Session.get('editMode');
    },
    selectedFlightId: function () {
        return Session.get('selectedFlightId');
    },
    selectedFlight: function () {
        var selectedFlightId = Session.get('selectedFlightId');
        return ClientFlights.findOne({_id: selectedFlightId});
    },
    selectedFlightData: function () {
        var airline = AutoForm.getFieldValue("airline", "clientFlightRecord");
        var flight_number = AutoForm.getFieldValue("flight_number", "clientFlightRecord");
        var scheduled_departure_date = AutoForm.getFieldValue("scheduled_departure_date", "clientFlightRecord");
        //console.log(airline);
        //console.log(flight_number);
        //console.log(scheduled_departure_date);

        if (scheduled_departure_date) {
            var schdDepDate = scheduled_departure_date.slice(0, 10);
            //console.log(schdDepDate);
            //{'schdDepDate': {'$regex': '2015-12-31'} }
            var flightRecord = Flights.findOne({'schdDepDate': {'$regex': schdDepDate} });
            if (flightRecord) {
                Session.set("selectedFlighDatatId", flightRecord._id);
                return flightRecord;
            }

        }


    },
    selectedClaim: function () {
        var selectedClaimId = Session.get('selectedClaimId');
        return ClientForms.findOne({_id: selectedClaimId});
    },
    clientFlights: function () {
        var clientPhoneNumber = Session.get('clientPhoneNumber');
        if (clientPhoneNumber) {
            var clientRecords = ClientFlights.find({phone: clientPhoneNumber}).fetch();
            //console.log("clientFlights: ", clientRecords);
            return clientRecords
        }
    },
    clientClaims: function () {
        var selectedFlightId = Session.get('selectedFlightId');
        if (selectedFlightId) {
            var clientClaims = ClientForms.find({clientFlightId: selectedFlightId}).fetch();
            //console.log(selectedFlightId);
            //console.log("clientClaims: ", clientClaims);
            return clientClaims
        }
    },
    autoFillSettings: function () {
        return {
            position: "bottom",
            limit: 5,
            rules: [
                {
                    collection: ClientFlights,
                    field: "phone",
                    template: Template.userPill
                }
            ]
        };
    },
    prefillFormData: function () {
        var preFillObj = {};
        preFillObj.client = {};
        preFillObj.client.telephone = {};
        // get Name
        var clientName = AutoForm.getFieldValue("name", "clientFlightRecord");
        // get email
        preFillObj.client.email = AutoForm.getFieldValue("email", "clientFlightRecord");
        preFillObj.client.telephone.number = AutoForm.getFieldValue("phone", "clientFlightRecord");
        //preFillObj.dob = AutoForm.getFieldValue("dob", "clientFlightRecord");
        preFillObj.airline = AutoForm.getFieldValue("airline", "clientFlightRecord");
        preFillObj.flightNumber = AutoForm.getFieldValue("flight_number", "clientFlightRecord");
        preFillObj.scheduledDepartureDate = AutoForm.getFieldValue("scheduled_departure_date", "clientFlightRecord");
        preFillObj.flight_value = AutoForm.getFieldValue("flight_value", "clientFlightRecord");

        // Split name
        if (clientName) {
            // break full name into first/last
            var space = clientName.indexOf(" ");
            preFillObj.client.firstname = clientName.slice(0, space);
            preFillObj.client.surname = clientName.slice(space + 1);
        }
        Session.set("preFillObj", preFillObj);
        return preFillObj

    },
    insertData: function () {
        return Session.get("insertData")
    }
});

Template.agentFlow.events({
    'submit .search-flights': function (event, t) {
        // Prevent default browser form submit
        event.preventDefault();
        var clientPhoneNumber = event.target.clientPhone.value;
        //console.log(clientPhoneNumber);
        Session.set('clientPhoneNumber', clientPhoneNumber);
        // reset
        // selectedFlightId
        // selectedClaimId
        Session.set('selectedFlightId', null);
        Session.set('selectedClaimId', null);
        Session.set('preFillObj', null);
        //Session.set('newMode', null);
    },
    'click #resetBtn': function (event) {
        Session.set('clientPhoneNumber', null);
        Session.set('selectedFlightId', null);
        Session.set('selectedClaimId', null);
        Session.set('newMode', null);
        Session.set('preFillObj', null);
        Session.set('insertData', null);
        Session.set('selectedFlighDatatId', null);
    },
    'click #newClaimLink': function (event) {
        Session.set('newMode', true);
        // reset selected claim
        Session.set('selectedClaimId', null);
        Tracker.nonreactive(function () {
            var fillData = Session.get("preFillObj");
            fillData.clientFlightId = Session.get('selectedFlightId');
            fillData.flightId = Session.get('selectedFlighDatatId');
            Session.set("insertData", fillData)
        })
    }
});

Template.clientFlight.events({
    'click .selectFlight': function (event) {
        //console.log(event);
        Session.set('selectedFlightId', event.target.id);
    }
});

Template.clientFlight.helpers({
    flightSelected: function (id) {
        var selectedFlightId = Session.get('selectedFlightId');
        if (id === selectedFlightId) {
            return true
        }
    },
    shortDate: function (date) {
        return moment(date).format("MM/DD/YYYY");
    },
    checkForCompletedClaims: function(clientFlightId){
        //console.log(clientFlightId);
        if (ClientForms.find({'clientFlightId': clientFlightId, formComplete: true }).fetch().length)
        {
            return true
        }

    }
});

Template.clientClaim.events({
    'click .selectClaim': function (event, t) {
        //console.log(event);
        //console.log(t);
        Session.set('selectedClaimId', event.target.id);
        //t.$('#editClaimFormBody').collapse("show");
        //t.$('.collapse').collapse();
    }
});
Template.clientClaim.helpers({
    shortDateTime: function (date) {
        return moment(date).format("MM/DD/YYYY - HH:mm (ZZ)");
    },
    claimSelected: function (id) {
        var selectedClaimId = Session.get('selectedClaimId');
        if (id === selectedClaimId) {
            return true
        }
    }
});


var uploader = new ReactiveVar();
var currentUserId = Meteor.userId();


Template.imageUploader.events({
    'change .uploadFile': function (event, template) {

        event.preventDefault();

        var upload = new Slingshot.Upload("images");
        var timeStamp = Math.floor(Date.now());
        var formId = Session.get('selectedClaimId');

        upload.send(document.getElementById('uploadFile').files[0], function (error, downloadUrl) {
            uploader.set();
            if (error) {
                //console.error('Error uploading');
                Notifications.error("Error uploading", error);
                //alert (error);
            }
            else {
                Notifications.success("Success!", 'uploaded file available here: ' + downloadUrl);
                //console.log("Success!");
                //console.log('uploaded file available here: ' + downloadUrl);
                Images.insert({
                    imageurl: downloadUrl,
                    time: timeStamp,
                    uploadedBy: currentUserId,
                    formId: formId
                });
                // update claim form
                ClientForms.update({
                    _id: formId
                }, {
                    $push: {
                        uploads: {
                            url: downloadUrl
                        }
                    }
                }, {upsert: false});

            }
        });
        uploader.set(upload);
    }
});

Template.imageUploader.helpers({

    isUploading: function () {
        return Boolean(uploader.get());
    },

    progress: function () {
        var upload = uploader.get();
        if (upload)
            return Math.round(upload.progress() * 100);
    },

    url: function () {

        return Images.findOne({uploadedBy: currentUserId}, {sort: {time: -1}});

    }

});
