Template.clientFormsList.helpers({
    allClaims: function() {
        return ClientForms.find().fetch();
    }
});

Template.clientFormsList.events({

});

Template.claimItem.events({
    'click .generateTemplate': function (event, t) {
        if (!event) {
            return
        }
        Session.set("templateClaimId", event.target.id);
        Modal.show("claimTemplateModal");
    }
});

Template.claimTemplateModal.helpers({
    calimId: function() {
        return Session.get("templateClaimId");
    },
    fillObj: function() {
        var claimId = Session.get("templateClaimId");
        if (claimId) {
            var fillObj = {};
            fillObj.claim = ClientForms.findOne({_id: claimId});

            if (fillObj.claim) {
                var amountOfPeople = (fillObj.claim.client.companions) ? fillObj.claim.client.companions.length : 0;

                fillObj.claim.totalValue =  fillObj.claim.flight_value * (amountOfPeople + 1);
                fillObj.claim.totalTicketValue =  fillObj.claim.ticketPrice * (amountOfPeople + 1);
                fillObj.claim.hadExpenses = fillObj.claim.expenses && fillObj.claim.expenses.length;

                // get flight details
                fillObj.flight = Flights.findOne({_id: fillObj.claim.flightId});

                // get airline
                fillObj.airline = Airlines.findOne({IATA: fillObj.claim.airline});
            }
            fillObj.date = new Date;

            if (fillObj.flight) {
                // calculate Delay
                var actArrDate = moment(fillObj.flight.actArrDate).unix();
                var schdArrDate = moment(fillObj.flight.schdArrDate).unix();
                var arrivalTimeDiff = actArrDate - schdArrDate;
                fillObj.flight.actDelayHours = (moment.duration(arrivalTimeDiff, 'seconds').asHours()).toFixed(2);
                // calculate Distance
               // get airports
                fillObj.arrApt = Airports.findOne({IATA_FAA: fillObj.flight.arrAptCode});
                fillObj.depApt = Airports.findOne({IATA_FAA: fillObj.flight.depAptCode});
                fillObj.distance = calculateDistance(fillObj.arrApt.Latitude, fillObj.arrApt.Longitude,
                  fillObj.depApt.Latitude, fillObj.depApt.Longitude, "K");
                //console.log(fillObj.distance);
                //debugger;
            }
            //console.log(fillObj);
            return fillObj;
        }
    }
});

function calculateDistance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1 / 180;
    var radlat2 = Math.PI * lat2 / 180;
    var theta = lon1 - lon2;
    var radtheta = Math.PI * theta / 180;
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit == "K") {
        dist = dist * 1.609344
    }
    if (unit == "N") {
        dist = dist * 0.8684
    }
    return dist.toFixed(0);
}