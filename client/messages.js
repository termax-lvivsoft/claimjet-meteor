serverMessages.listen('serverMessage:info', function (subject, message, options) {
    Notifications.info(subject, message, options);
});

serverMessages.listen('serverMessage:warning', function (subject, message, options) {
    Notifications.warn(subject, message, options);
});

serverMessages.listen('serverMessage:success', function (subject, message, options) {
    Notifications.success(subject, message, options);
});

serverMessages.listen('serverMessage:error', function (subject, message, options) {
    Notifications.error(subject, message, options);
});

Meteor.startup(function () {
    Notifications.settings.animationSpeed = 500;
    _.extend(Notifications.defaultOptions, {
        timeout: 3000
    });
});