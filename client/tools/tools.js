Session.setDefault("amountOfAgents", 1);

Template.tools.events({
    'click #caspioGetRecords': function (event) {
        //console.log("caspioGetRecords button clicked");
        Meteor.call('caspioGetRecords', function (error, result) {
            if (error) {
                console.log(error)
            }
        });
    },
    'click #getFlights': function (event) {
        //console.log("getFlights button clicked");
        Meteor.call('getFlights', function (error, result) {
            if (error) {
                console.log(error)
            } else {
                //console.log(result)
            }
        });
    },
    'click #getAirlines': function (event) {
        //console.log("getFlights button clicked");
        Meteor.call('getAirlines', function (error, result) {
            if (error) {
                console.log(error)
            } else {
                //console.log(result)
            }
        });
    },
    'click #getAirports': function (event) {
        //console.log("getFlights button clicked");
        Meteor.call('getAirports', function (error, result) {
            if (error) {
                console.log(error)
            } else {
                //console.log(result)
            }
        });
    },
    'click #getTourRecords': function (event) {
        console.log("getTourRecords button clicked");

        Meteor.call('getTourRecords', function (error, result) {
            if (error) {
                console.log(error)
            }
        });
    },
    'click #testButton': function (event) {
        //console.log("readingFile button ", this);
        Meteor.call('sendLogMessage', function (error, result) {
            console.log("err: ", error);
            console.log("result: ", result);
        });
    },
    'click #testButton2': function (event) {
        //console.log("readingFile button ", this);
        Meteor.call('oag2', function (error, result) {
            console.log("err: ", error);
            console.log("result: ", result);
        });
    }
});

Template.tools.helpers({

    agentCount: function () {
      return Credentials.find().count();
    },
    agentCredentials: function () {
        return Credentials.find().fetch();
    },
    clientImportedCount: function () {
        return ClientFlights.find().count();
    },
    flightImportedCount: function () {
        return Flights.find().count();
    },
    airlineImportedCount: function () {
        return Airlines.find().count();
    },
    airportImportedCount: function () {
        return Airports.find().count();
    }
});

Template.generateAgents.helpers({
    amountOfAgents: function () {
        return Session.get("amountOfAgents")
    }
});
Template.generateAgents.events({
    'keyup input': function (event) {
        Session.set("amountOfAgents", event.target.value);
    },
    'click #generateAgents': function (event) {
        var amount = Session.get("amountOfAgents");
        //console.log("generateAgents button clicked");
        Meteor.call('generateAgents', amount);
    }
});

Template.generateAgent.helpers({
    usernameOfAgent: function () {
        return Session.get("usernameOfAgent")
    }
});

Template.generateAgent.events({
    'keyup input': function (event) {
        Session.set("usernameOfAgent", event.target.value);
    },
    'click #generateAgent': function (event) {
        var username = Session.get("usernameOfAgent");
        //console.log("generateAgents button clicked");
        Meteor.call('generateAgent', username);
    }
});

