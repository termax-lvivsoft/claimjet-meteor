Flights = new Mongo.Collection('flights');

// Attach Schema

Flights.attachSchema(FlightsSchema);

// Permissions

Flights.allow({
    insert: function(userId){
        return userId;
    },
    update: function(userId){
        return userId;
    },
    remove: function(userId){
        return userId;
    }
});