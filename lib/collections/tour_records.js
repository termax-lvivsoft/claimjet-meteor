TourRecords = new Mongo.Collection('tourRecords');

// Attach Schema

TourRecords.attachSchema(TourRecordsSchema);

// Permissions

TourRecords.allow({
    insert: function(userId){
        return userId;
    },
    update: function(userId){
        return userId;
    },
    remove: function(userId){
        return userId;
    }
});