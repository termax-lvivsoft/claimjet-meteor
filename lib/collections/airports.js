Airports = new Mongo.Collection('airports');

// Attach Schema

Airports.attachSchema(AirportsSchema);

// Permissions

Airports.allow({
    insert: function(userId){
        return userId;
    },
    update: function(userId){
        return userId;
    },
    remove: function(userId){
        return userId;
    }
});