Credentials = new Mongo.Collection('credentials');

// Attach Schema

Credentials.attachSchema(CredentialsSchema);

// Permissions

Credentials.allow({
    insert: function(userId){
        return userId;
    },
    update: function(userId){
        return userId;
    },
    remove: function(userId){
        return userId;
    }
});