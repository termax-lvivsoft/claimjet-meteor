Clients = new Mongo.Collection('clients');

// Attach Schema

Clients.attachSchema(ClientsSchema);

// Permissions

Clients.allow({
    insert: function(userId){
        return userId;
    },
    update: function(userId){
        return userId;
    },
    remove: function(userId){
        return userId;
    }
});




