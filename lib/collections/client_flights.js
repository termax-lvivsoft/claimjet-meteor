ClientFlights = new Mongo.Collection('clientFlights');

// Attach Schema

ClientFlights.attachSchema(ClientFlightsSchema);



// Permissions

ClientFlights.allow({
    insert: function(userId){
        return userId;
    },
    update: function(userId){
        return userId;
    },
    remove: function(userId){
        return userId;
    }
});