Airlines = new Mongo.Collection('airlines');

// Attach Schema

Airlines.attachSchema(AirlinesSchema);

// Permissions

Airlines.allow({
    insert: function(userId){
        return userId;
    },
    update: function(userId){
        return userId;
    },
    remove: function(userId){
        return userId;
    }
});