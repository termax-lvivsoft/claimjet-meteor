ClientForms = new Mongo.Collection('clientForms');

// Attach Schema

ClientForms.attachSchema(ClientFormsSchema);

// Permissions

ClientForms.allow({
    insert: function(userId){
        return userId;
    },
    update: function(userId){
        return userId;
    },
    remove: function(userId){
        return userId;
    }
});