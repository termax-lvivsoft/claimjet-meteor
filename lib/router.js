//Router.configure({
//    layoutTemplate: 'layoutManage'
//});
//
//Router.configure({
//    layoutTemplate: "layoutClient"
//});

Router.configure({
    loadingTemplate: 'loading'
});


// Index
Router.route('/', {
      name: 'homeIndex',
      action: function () {
          this.layout('layoutHome');
          this.render('homeIndex');
      }
      //waitOn: function () {
      //    var wf = document.createElement('script');
      //    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      //      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
      //    wf.type = 'text/javascript';
      //    wf.async = 'true';
      //    var s = document.getElementsByTagName('script')[0];
      //    s.parentNode.insertBefore(wf, s);
      //    //console.log("async fonts loaded", WebFontConfig);
      //}
  }
);

Router.route('/manage', {
      name: 'manageIndex',
      action: function () {
          this.layout('layoutManage');
          this.render('manageIndex');
      }
  }
);

Router.route('/agent', {
      name: 'agentIndex',
      action: function () {
          this.layout('layoutAgent');
          this.render('agentIndex');
      }
  }
);

Router.route('/tools', {
      name: 'tools',
      action: function () {
          this.layout('layoutManage');
          this.render('tools');
      },
      subscriptions: function () {
          // returning a subscription handle or an array of subscription handles
          // adds them to the wait list.
          return [
              Meteor.subscribe('credentials'),
              Meteor.subscribe('clientFlights'),
              Meteor.subscribe('flights'),
              Meteor.subscribe('airlines'),
              Meteor.subscribe('airports')

          ]
      }
  }
);

// Agent Flow

Router.route('/agentFlow', {
    name: 'agentFlow',
    layoutTemplate: 'layoutAgent',
    subscriptions: function () {
        // returning a subscription handle or an array of subscription handles
        // adds them to the wait list.
        return [
            Meteor.subscribe('clientFlights'),
            Meteor.subscribe('clientForms'),
            Meteor.subscribe('images'),
            Meteor.subscribe('flights')
        ]
    }

});

// Clients

Router.route('/clients', {
    name: 'clientsList',
    layoutTemplate: 'layoutManage',
    subscriptions: function () {
        // returning a subscription handle or an array of subscription handles
        // adds them to the wait list.
        return [
            Meteor.subscribe('clients')
        ]
    }
});


Router.route('/client/:_id', function () {
    var client = clients.findOne({_id: this.params._id});
    this.layout('layoutManage');
    this.render('clientFormShow', {data: client});
});

// Client Forms

Router.route('/client_forms', {
    name: 'clientFormsList',
    layoutTemplate: 'layoutManage',
    subscriptions: function () {
        // returning a subscription handle or an array of subscription handles
        // adds them to the wait list.
        return [
            Meteor.subscribe('clientForms'),
            Meteor.subscribe('flights'),
            Meteor.subscribe('airlines'),
            Meteor.subscribe('airports')
        ]
    }
});


Router.route('/client_forms/:_id', function () {
    var clientForm = clientForms.findOne({_id: this.params._id});
    this.layout('layoutManage');
    this.render('clientFormShow', {data: clientForm});
});

// Agent accounts csv download

Router.map(function () {
    this.route('outputAccounts', {
        where: 'server',
        name: 'outputAccounts',
        path: '/outputAccounts',
        action: function () {
            var text = "";
            var output = Credentials.find().fetch();
            output.forEach(function (result) {
                if (result) {
                    text += result.username + "," + result.password + "\n";
                }
            });
            var filename = 'accounts' + '.csv';

            var headers = {
                'Content-Type': 'text/plain',
                'Content-Disposition': "attachment; filename=" + filename
            };

            this.response.writeHead(200, headers);
            return this.response.end(text);
        }
    });
});

// API

Router.map(function () {
    this.route('claimjetApi', {
        path: '/api/call',
        where: 'server',
        action: function () {
            // GET, POST, PUT, DELETE
            var requestMethod = this.request.method;
            // Data from a POST request
            var requestData = this.request.body;
            // Could be, e.g. application/xml, etc.
            this.response.writeHead(200, {'Content-Type': 'text/html'});
            this.response.end('<html><body>Your request was a ' + requestMethod + '</body></html>');
        }
    });
});


Router.onBeforeAction(function () {
      if (!Meteor.userId()) {
          this.render('restricted');
          this.layout('restricted');
      } else {
          var loggedInUser = Meteor.user();
          if (Roles.userIsInRole(loggedInUser, ['admin', 'operator', 'manager'])) {
              // NOTE: This example assumes the user is not using groups.
              this.next();
          } else {
              this.render('restricted');
              this.layout('restricted');
          }

      }
  },
  {
      except: ['homeIndex', 'manageIndex', 'agentIndex']
  }
);

Router.onBeforeAction(function () {
      if (!Meteor.userId()) {
          this.render('restricted');
          this.layout('restricted');
      } else {
          var loggedInUser = Meteor.user();
          if (Roles.userIsInRole(loggedInUser, ['admin'])) {
              // NOTE: This example assumes the user is not using groups.
              this.next();
          } else {
              this.render('restricted');
              this.layout('restricted');
          }

      }
  },
  {
      only: ['tools']
  }
);

Router.onBeforeAction(function () {
      if (!Meteor.userId()) {
          this.render('restricted');
          this.layout('restricted');
      } else {
          var loggedInUser = Meteor.user();
          if (Roles.userIsInRole(loggedInUser, ['admin', 'manager'])) {
              // NOTE: This example assumes the user is not using groups.
              this.next();
          } else {
              this.render('restricted');
              this.layout('restricted');
          }

      }
  },
  {
      only: ['clientFormsList']
  }
);
