ExpenseSchema = new SimpleSchema({
    expense: {
        type: String,
        label: "Expense",
        optional: true
    },
    amount: {
        type: Number,
        label: "Amount",
        optional: true
    },
    currency: {
        type: String,
        label: "Currency",
        optional: true
    }
});