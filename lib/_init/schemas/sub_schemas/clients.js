// Sub Schemas
ClientsAddressSchema = new SimpleSchema({
    houseNumber: {
        type: String,
        label: "House Number / Name",
        optional: true
    },
    street: {
        type: String,
        label: "Street / Road",
        optional: true
    },
    town: {
        type: String,
        label: "Town / City",
        optional: true
    },
    postcode: {
        type: String,
        label: "Postcode",
        optional: true
    },
    county: {
        type: String,
        label: "County",
        optional: true
    },
    country: {
        type: String,
        label: "Country",
        optional: true
    }

});

ClientsTelephoneSchema = new SimpleSchema({
    number: {
        type: String,
        label: "Telephone",
        optional: true
    },
    type: {
        type: String,
        label: "Tel Type",
        optional: true
    }
});

ClientsCompanionSchema = new SimpleSchema({
    fullName: {
        type: String,
        label: "Full Name",
        optional: true
    },
    dob: {
        type: String,
        label: "Date of Birth",
        optional: true
    },
    relationship: {
        type: String,
        label: "Relationship of main applicant to this person",
        optional: true
    },
    passport: {
        type: String,
        label: "Passport Number",
        optional: true
    }
});





