DocumentSchema = new SimpleSchema({
    url: {
        type: String,
        label: "Image",
        optional: false,
        autoform: {
            afFieldInput: {
                type: "imageS3"
            }
        }
    },
    name: {
        type: String,
        label: "Name/Note",
        optional: true
    },
    type: {
        type: String,
        label: "Type",
        optional: true,
        autoform: {
            options: {
                passport: "Passport",
                boarding: "Boarding Pass",
                receipt: "Receipt",
                itinerary: "Itinerary",
                companionPassport: "Document of Companion"
            }
        }
    }
});