// Define the schema
ClientsSchema = new SimpleSchema({
    title: {
        type: String,
        label: "Title",
        optional: true
    },
    surname: {
        type: String,
        label: "Surname",
        optional: true
    },
    firstname: {
        type: String,
        label: "First Name",
        optional: true
    },
    passport: {
        type: String,
        label: "Passport",
        optional: true
    },
    email: {
        type: String,
        label: "e-mail",
        optional: true
    },
    address: {
        type: ClientsAddressSchema,
        label: "Address",
        optional: true
    },
    telephone: {
        type: ClientsTelephoneSchema,
        label: "Telephone",
        optional: true
    },
    companions: {
        type: [ClientsCompanionSchema],
        label: "Accompanied by",
        optional: true
    }
});





