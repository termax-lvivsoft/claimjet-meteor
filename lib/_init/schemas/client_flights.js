ClientFlightsSchema = new SimpleSchema({
    PK_ID: {
        type: Number,
        label: "Caspio ID",
        optional: true
    },
    name: {
        type: String,
        label: "Full Name",
        optional: true
    },
    email: {
        type: String,
        label: "e-mail",
        optional: true
    },
    phone: {
        type: String,
        label: "Phone",
        optional: true
    },
    created: {
        type: String,
        label: "Created Date",
        optional: true
    },
    application_complete: {
        type: Boolean,
        label: "Application Complete",
        optional: true
    },
    primary_key: {
        type: Number,
        label: "Primary Key",
        optional: true
    },
    dob: {
        type: String,
        label: "Date of Birth",
        optional: true
    },
    circumstances_option: {
        type: String,
        label: "Circumstance Option",
        optional: true
    },
    airline: {
        type: String,
        label: "Airline",
        optional: true
    },
    package_holiday: {
        type: String,
        label: "Package Holiday",
        optional: true
    },
    departure_airport: {
        type: String,
        label: "Departure Airport",
        optional: true
    },
    destination_airport: {
        type: String,
        label: "Destination Airport",
        optional: true
    },
    flight_number: {
        type: String,
        label: "Flight number",
        optional: true
    },
    scheduled_departure_date: {
        type: String,
        label: "Scheduled Departure Date",
        optional: true
    },
    actual_departure_date: {
        type: String,
        label: "Actual Departure Date",
        optional: true
    },
    booking_ref: {
        type: String,
        label: "Booking Reference",
        optional: true
    },
    postcode: {
        type: String,
        label: "Postal Code",
        optional: true
    },
    house_number: {
        type: String,
        label: "House Number",
        optional: true
    },
    street: {
        type: String,
        label: "Street",
        optional: true
    },
    city: {
        type: String,
        label: "City",
        optional: true
    },
    county: {
        type: String,
        label: "County",
        optional: true
    },
    country: {
        type: String,
        label: "Country",
        optional: true
    },
    received_compensation: {
        type: String,
        label: "Received Compensation",
        optional: true
    },
    offered_accomodation: {
        type: String,
        label: "Offered Accommodation",
        optional: true
    },
    additional_info: {
        type: String,
        label: "Additional Info",
        optional: true
    },
    terms: {
        type: String,
        label: "Terms",
        optional: true
    },
    scheduled_arrival_time: {
        type: String,
        label: "Scheduled Arrival Time",
        optional: true
    },
    actual_arrival_time: {
        type: String,
        label: "Actual Arrival Time",
        optional: true
    },
    CaseID: {
        type: String,
        label: "CaseID",
        optional: true
    },
    jsignature: {
        type: String,
        label: "JS Signature",
        optional: true
    },
    pdf_public_url: {
        type: String,
        label: "pdf public url",
        optional: true
    },
    callhub_flag: {
        type: String,
        label: "callhub flag",
        optional: true
    },
    flight_value: {
        type: String,
        label: "Flight Value",
        optional: true
    },
    pipedrive_id: {
        type: String,
        label: "PipeDrive Id",
        optional: true
    }


});
