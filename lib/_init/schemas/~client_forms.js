// Define the schema
ClientFormsSchema = new SimpleSchema({
    client: {
        type: ClientsSchema,
        label: "Client",
        optional: true
    },
    confirmationForCompanions: {
        type: Boolean,
        label: "Legal authorization gained from client for accompanied by (if selected)",
        optional: true
    },
    circumstances: {
        type: String,
        label: "How would you best describe your circumstances",
        optional: true
    },
    scheduledDepartureDate: {
        type: String,
        label: "Scheduled departure date",
        optional: true
    },
    airline: {
        type: String,
        label: "Airline code",
        optional: true
    },
    flightNumber: {
        type: String,
        label: "Flight number (Carrier Code + number)",
        optional: true
    },
    bookingReference: {
        type: String,
        label: "Booking reference (if known):",
        optional: true
    },
    didntFly: {
        type: Boolean,
        label: "Did you decide not to fly",
        optional: true
    },
    ticketPrice: {
        type: String,
        label: "Ticket Price (if decide not to fly)",
        optional: true
    },
    compensationReceived: {
        type: String,
        label: "Have you received any compensation (money/vouchers) already?",
        optional: true
    },
    freeStuffReceived: {
        type: String,
        label: "Were you offered free food and drinks or accommodation in the case of a long overnight delay?",
        optional: true
    },
    expenses: {
        type: Array,
        label: "Expenses",
        optional: true,
        autoform: {
            label: "Did you spend any money on food and drinks or accommodation in the case of a long overnight delay?"
                //template: 'expenses'
        }
    },
    'expenses.$': {
        type: ExpenseSchema,
        label: "Expense",
        optional: true,
        autoform: {
            label: "Expense"
            //template: 'expense'
        }
    },
    otherInfo: {
        type: String,
        label: "Is there any other information you wish to tell us?",
        optional: true
    },
    confim: {
        type: Boolean,
        label: "Legal authorization gained from client",
        optional: true
    },
    flight_value: {
        type: Number,
        label: "Flight Value",
        optional: true,
        autoform: {
            type: "hidden"
        }
    },
    formComplete: {
        type: Boolean,
        label: "Please mark when this claim form is completely filled",
        optional: true
    },
    uploads: {
        type: Array,
        optional: true,
        label: "Document Uploads"
    },
    'uploads.$': {
        type: DocumentSchema,
        optional: true,
        label: "Document"
    },
    startedDate: {
        type: Date,
        autoform: {
            type: "hidden"
        },
        label: "Date of creation",
        autoValue: function () {
            if (this.isInsert) {
                return new Date;
            } else if (this.isUpsert) {
                return {$setOnInsert: new Date};
            } else {
                this.unset();  // Prevent user from supplying their own value
            }
        }
    },
    agentId: {
        type: String,
        autoform: {
            type: "hidden"
        },
        autoValue: function(){
            //console.log("user", this.userId);
            return this.userId
        }
    },
    clientFlightId: {
        type: String,
        autoform: {
            type: "hidden"
        }
    },
    flightId: {
        type: String,
        autoform: {
            type: "hidden"
        },
        optional: true
    }


});
