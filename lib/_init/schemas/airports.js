AirportsSchema = new SimpleSchema({
    AirportID: {
        type: Number,
        label: "AirportID",
        optional: true
    },
    Name: {
        type: String,
        label: "Name",
        optional: true
    },
    City: {
        type: String,
        label: "City",
        optional: true
    },
    Country: {
        type: String,
        label: "Country",
        optional: true
    },
    IATA_FAA: {
        type: String,
        label: "IATA_FAA",
        optional: true
    },
    ICAO: {
        type: String,
        label: "ICAO",
        optional: true
    },
    Latitude: {
        type: String,
        label: "Latitude",
        optional: true
    },
    Longitude: {
        type: String,
        label: "Longitude",
        optional: true
    },
    Altitude: {
        type: Number,
        label: "Altitude",
        optional: true
    },
    Timezone: {
        type: Number,
        label: "Timezone",
        optional: true
    },
    DST: {
        type: String,
        label: "DST",
        optional: true
    },
    EU: {
        type: Number,
        label: "EU",
        optional: true
    },
    missing: {
        type: Boolean,
        label: "missing",
        optional: true,
        autoform: {
            hidden: true
        }
    }
});


//`  AirportID`         int(11) DEFAULT NULL,
//`  Name`              varchar(255) DEFAULT NULL,
//`  City`              varchar(255) DEFAULT NULL,
//`  Country`           varchar(255) DEFAULT NULL,
//`  IATA_FAA`          varchar(255) DEFAULT NULL,
//`  ICAO`              varchar(255) DEFAULT NULL,
//`  Latitude`          varchar(255) DEFAULT NULL,
//`  Longitude`         varchar(255) DEFAULT NULL,
//`  Altitude`          int(11) DEFAULT NULL,
//`  Timezone`          int(11) DEFAULT NULL,
//`  DST`               varchar(255) DEFAULT NULL,
//`  EU`                int(11) DEFAULT NULL