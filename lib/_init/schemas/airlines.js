AirlinesSchema = new SimpleSchema({
    AirlineId: {
        type: Number,
        label: "AirlineId",
        optional: true
    },
    Name: {
        type: String,
        label: "Name",
        optional: true
    },
    Alias: {
        type: String,
        label: "Alias",
        optional: true
    },
    IATA: {
        type: String,
        label: "IATA",
        optional: true
    },
    ICAO: {
        type: String,
        label: "ICAO",
        optional: true
    },
    Callsign: {
        type: String,
        label: "Callsign",
        optional: true
    },
    Country: {
        type: String,
        label: "Country",
        optional: true
    },
    Active: {
        type: String,
        label: "Active",
        optional: true
    },
    EU: {
        type: Number,
        label: "EU",
        optional: true
    },
    Address: {
        type: ClientsAddressSchema,
        label: "Address",
        optional: true
    },
    missing: {
        type: Boolean,
        label: "missing",
        optional: true,
        autoform: {
            hidden: true
        }
    }
});


  //  `AirlineId`     int(11) DEFAULT NULL,
  //  `Name`          varchar(255) DEFAULT NULL,
  //  `Alias`         varchar(255) DEFAULT NULL,
  //  `IATA`          varchar(255) DEFAULT NULL,
  //  `ICAO`          varchar(255) DEFAULT NULL,
  //  `Callsign`      varchar(255) DEFAULT NULL,
  //  `Country`       varchar(255) DEFAULT NULL,
  //  `Active`        varchar(255) DEFAULT NULL,
  //  `EU`            int(11) DEFAULT NULL