// Define the schema
CredentialsSchema = new SimpleSchema({
    username: {
        type: String,
        label: "Username",
        optional: true
    },
    password: {
        type: String,
        label: "Password",
        optional: true
    },
    userId: {
        type: String,
        label: "Password",
        optional: true
    }
});