// Define the schema
FlightsSchema = new SimpleSchema({
    loc_id: {
        type: Number,
        label: "sql_id",
        optional: true
    },
    schdDepDate: {
        type: String,
        label: "schdDepDate",
        optional: true
    },
    schdArrDate: {
        type: String,
        label: "schdArrDate",
        optional: true
    },
    arrivalAirport: {
        type: String,
        label: "arrivalAirport",
        optional: true
    },
    arrStatus: {
        type: String,
        label: "arrStatus",
        optional: true
    },
    actDepDate: {
        type: String,
        label: "actDepDate",
        optional: true
    },
    flightNumber: {
        type: String,
        label: "flightNumber",
        optional: true
    },
    arrSubStatus: {
        type: String,
        label: "arrSubStatus",
        optional: true
    },
    depSubStatus: {
        type: String,
        label: "depSubStatus",
        optional: true
    },
    carrierCode: {
        type: String,
        label: "carrierCode",
        optional: true
    },
    aircraftName: {
        type: String,
        label: "aircraftName",
        optional: true
    },
    arrDelayReasonDesc: {
        type: String,
        label: "arrDelayReasonDesc",
        optional: true
    },
    depAptCode: {
        type: String,
        label: "depAptCode",
        optional: true
    },
    depStatus: {
        type: String,
        label: "depStatus",
        optional: true
    },
    actArrDate: {
        type: String,
        label: "actArrDate",
        optional: true
    },
    arrDelayReason: {
        type: String,
        label: "arrDelayReason",
        optional: true
    },
    arrAptCode: {
        type: String,
        label: "arrAptCode",
        optional: true
    },
    carrierName: {
        type: String,
        label: "carrierName",
        optional: true
    },
    depDelayReason: {
        type: String,
        label: "depDelayReason",
        optional: true
    },
    depTerminal: {
        type: String,
        label: "depTerminal",
        optional: true
    },
    depDelayReasonDesc: {
        type: String,
        label: "depDelayReasonDesc",
        optional: true
    },
    fltLegSeqNo: {
        type: String,
        label: "fltLegSeqNo",
        optional: true
    },
    departAirport: {
        type: String,
        label: "departAirport",
        optional: true
    },
    id: {
        type: Number,
        label: "oag_id",
        optional: true
    },
    missing: {
        type: Boolean,
        label: "missing",
        optional: true,
        autoform: {
            hidden: true
        }
    }
});

//  `loc_id`                int(10) unsigned NOT NULL AUTO_INCREMENT,
//  `schdDepDate`           varchar(255) DEFAULT NULL,
//  `schdArrDate`           varchar(255) DEFAULT NULL,
//  `arrivalAirport`        varchar(255) DEFAULT NULL,
//  `arrStatus`             varchar(255) DEFAULT NULL,
//  `actDepDate`            varchar(255) DEFAULT NULL,
//  `flightNumber`          varchar(255) DEFAULT NULL,
//  `arrSubStatus`          varchar(255) DEFAULT NULL,
//  `depSubStatus`          varchar(255) DEFAULT NULL,
//  `carrierCode`           varchar(255) DEFAULT NULL,
//  `aircraftName`          varchar(255) DEFAULT NULL,
//  `arrDelayReasonDesc`    varchar(255) DEFAULT NULL,
//  `depAptCode`            varchar(255) DEFAULT NULL,
//  `depStatus`             varchar(255) DEFAULT NULL,
//  `actArrDate`            varchar(255) DEFAULT NULL,
//  `arrDelayReason`        varchar(255) DEFAULT NULL,
//  `arrAptCode`            varchar(255) DEFAULT NULL,
//  `carrierName`           varchar(255) DEFAULT NULL,
//  `depDelayReason`        varchar(255) DEFAULT NULL,
//  `depTerminal`           varchar(255) DEFAULT NULL,
//  `depDelayReasonDesc`    varchar(255) DEFAULT NULL,
//  `fltLegSeqNo`           varchar(255) DEFAULT NULL,
//  `departAirport`         varchar(255) DEFAULT NULL,
//  `id`                    varchar(255) NOT NULL DEFAULT '',

