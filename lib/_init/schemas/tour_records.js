TourRecordsSchema = new SimpleSchema({
    firstName: {
        type: String,
        label: "First Name",
        optional: true
    },
    lastName: {
        type: String,
        label: "Last Name",
        optional: true
    },
    primaryNumber: {
        type: String,
        label: "Telephone Number",
        optional: true
    },
    destTo: {
        type: String,
        label: "Flying From",
        optional: true
    },
    destFrom: {
        type: String,
        label: "Flying From",
        optional: true
    },
    bookingDate: {
        type: String,
        label: "Booking Date",
        optional: true
    },
    carrier: {
        type: String,
        label: "Carrier",
        optional: true
    },
    flightNo: {
        type: String,
        label: "Flight Number",
        optional: true
    },
    travelDate: {
        type: String,
        label: "Date",
        optional: true
    },
    flightCategory: {
        type: String,
        label: "Flight Category",
        optional: true
    },
    oagId: {
        type: String,
        label: "OAG ID",
        optional: true
    }

});
